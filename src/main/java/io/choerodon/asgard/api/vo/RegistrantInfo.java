package io.choerodon.asgard.api.vo;

import io.swagger.annotations.ApiModelProperty;
import org.hzero.starter.keyencrypt.core.Encrypt;

public class RegistrantInfo {
    @ApiModelProperty(value = "注册人用户名")
    private String realName;

    @ApiModelProperty(value = "注册组织ID")
    private Long organizationId;

    @ApiModelProperty(value = "注册组织名称")
    private String organizationName;

    @ApiModelProperty(value = "客户成功")
    private User successManagerUser;
    @ApiModelProperty(value = "销售")
    private User markingManagerUser;

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public User getSuccessManagerUser() {
        return successManagerUser;
    }

    public void setSuccessManagerUser(User successManagerUser) {
        this.successManagerUser = successManagerUser;
    }

    public User getMarkingManagerUser() {
        return markingManagerUser;
    }

    public void setMarkingManagerUser(User markingManagerUser) {
        this.markingManagerUser = markingManagerUser;
    }

    @Override
    public String toString() {
        return "RegistrantInfo{" +
                ", realName='" + realName + '\'' +
                ", organizationId=" + organizationId +
                ", organizationName='" + organizationName + '\'' +
                '}';
    }
}
